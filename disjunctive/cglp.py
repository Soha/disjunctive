#
# Implement various CGLPs from the literature.
#
import abc
import cplex

from collections import OrderedDict


class CGLP(abc.ABC):
    @abc.abstractmethod
    def __init__(self):
        pass

    @abc.abstractmethod
    def get_cut(self, sol):
        pass

    def write(self, file_name):
        self.cpx.write(file_name)

    def get_coefs(self, xids):
        return self.cpx.solution.get_values(xids)

    def solve(self):
        self.cpx.solve()

        assert self.cpx.solution.get_status() == self.cpx.solution.status.optimal, (
            "CGLP is " + self.cpx.solution.get_status_string()
        )


class OneStep(CGLP):
    """ "One-step" CGLP from [2] based on the current solution. """

    def __init__(self, sol, A, b, pi, pi_0, trivial_norm=False):
        super().__init__()

        nvars = len(sol) + 1
        ncons = len(b) + 1

        self.nvars = nvars

        assert len(A) == len(b)
        assert len(sol) == len(A[0])
        assert len(pi) == len(sol)

        self.cpx = cplex.Cplex()

        gids = self.cpx.variables.add(
            obj=[-1] + sol,
            names=["g_{}".format(i) for i in range(nvars)],
            lb=[-cplex.infinity] * nvars,
        )
        uids = self.cpx.variables.add(
            names=["u_{}".format(i) for i in range(ncons)], lb=[0] * ncons
        )
        vids = self.cpx.variables.add(
            names=["v_{}".format(i) for i in range(ncons)], lb=[0] * ncons
        )

        for i in range(nvars - 1):
            ma = [-x[i] for x in A]
            # \pi is an elementary disjunction: 1 branching variable and 0 otw.
            #
            # \gamma + u constraint
            self.cpx.linear_constraints.add(
                [([gids[i + 1]] + list(uids), [1] + [pi[i]] + ma)], "E", [0]
            )
            # \gamma + v constraint, change sense of \pi
            self.cpx.linear_constraints.add(
                [([gids[i + 1]] + list(vids), [1] + [-pi[i]] + ma)], "E", [0]
            )

        mb = [-x for x in b]
        # \gamma_0 + u constraint
        self.cpx.linear_constraints.add(
            [([gids[0]] + list(uids), [1] + [pi_0] + mb)], "E", [0]
        )
        # \gamma_0 + v constraint
        self.cpx.linear_constraints.add(
            [([gids[0]] + list(vids), [1] + [-(pi_0 + 1)] + mb)], "E", [0]
        )

        # Cut normalisation
        if trivial_norm:
            # Trivial
            self.cpx.linear_constraints.add([([uids[0], vids[0]], [1, 1])], "E", [1])
        else:
            # Standard Normalisation Condition (SNC)
            self.cpx.linear_constraints.add(
                [(list(uids) + list(vids), [1] * (len(uids) * 2))], "E", [1]
            )

    def get_cut(self, sol):
        pi = self.cpx.solution.get_values(list(range(self.nvars)))

        return pi[1:], pi[0]


class M1NC(CGLP):
    """
    CGLP from Chen [3] which is based on the leaves of the CPT. We normalise it
    using the L1 norm. This means we linearise the objective variables: $\sum_i
    |\pi_i|$ in two: $\sum_i a_i + b_i$.
    """

    def __init__(self, sol, A, b, leaves):
        super().__init__()
        self.cpx = cplex.Cplex()

        nvars = len(sol)
        nleaves = len(leaves)
        ncons = len(b)
        b_km = []

        self.nvars = nvars

        for i, b_m in enumerate(b):
            b_km.append(b_m - sum(a * x for a, x in zip(A[i], sol)))

        assert len(A) == len(b)
        assert len(sol) == nvars

        self.cpx = cplex.Cplex()
        self.cpx.objective.set_sense(self.cpx.objective.sense.minimize)

        leaf_names = OrderedDict(
            ((l, j), "l_{}_{}".format(l, j))
            for j in range(ncons)
            for l in range(nleaves)
        )
        mu_names = OrderedDict(
            ((l, i), "mu_{}_{}".format(l, i))
            for i in range(nvars)
            for l in range(nleaves)
        )
        nu_names = OrderedDict(
            ((l, i), "nu_{}_{}".format(l, i))
            for i in range(nvars)
            for l in range(nleaves)
        )

        u_names = ["u_{}".format(i + 1) for i in range(nvars)]
        v_names = ["v_{}".format(i + 1) for i in range(nvars)]

        # Positive part of \pi
        uids = self.cpx.variables.add(obj=[1] * nvars, names=u_names, lb=[0] * nvars)
        # Negative part of \pi
        vids = self.cpx.variables.add(obj=[1] * nvars, names=v_names, lb=[0] * nvars)
        lids = self.cpx.variables.add(
            names=list(leaf_names.values()), lb=[0] * ncons * nleaves
        )
        muids = self.cpx.variables.add(
            names=list(mu_names.values()), lb=[0] * nvars * nleaves
        )
        nuids = self.cpx.variables.add(
            names=list(nu_names.values()), lb=[0] * nvars * nleaves
        )

        for l in range(nleaves):
            leaf = [leaf_names[l, j] for j in range(ncons)]
            for i in range(nvars):
                names = (
                    [u_names[i], v_names[i]] + leaf + [mu_names[l, i], nu_names[l, i]]
                )

                ma = [-x[i] for x in A]
                coefs = [1, -1] + ma + [-1, 1]
                self.cpx.linear_constraints.add([(names, coefs)], "E", [0])

            var_names = (
                leaf
                + [mu_names[l, i] for i in range(nvars)]
                + [nu_names[l, i] for i in range(nvars)]
            )
            L_k = [bound[0] - sol[i] for i, bound in enumerate(leaves[l])]
            U_k = [-(bound[1] - sol[i]) for i, bound in enumerate(leaves[l])]
            self.cpx.linear_constraints.add([(var_names, b_km + L_k + U_k)], "G", [1])

    def get_cut(self, sol):
        """
        Compute the vector \pi which is the sum (i.e., obj function) of the
        \alpha and \betas (i.e., linearised L1-norm)
        """
        pi = self.cpx.solution.get_values(list(range(self.nvars * 2)))
        p0 = 1
        for i in range(self.nvars):
            p0 += (pi[i] - pi[i + self.nvars]) * sol[i]

        return [pi[i] - pi[i + self.nvars] for i in range(self.nvars)], p0


class Leaf(CGLP):
    """ Default CGLP from Chen, we use Fischetti's normalisation. """

    def __init__(self, sol, A, b, leaves):
        super().__init__()
        self.cpx = cplex.Cplex()

        nvars = len(sol)
        nleaves = len(leaves)
        ncons = len(b)

        self.nvars = nvars + 1

        assert len(A) == len(b)

        self.cpx = cplex.Cplex()
        self.cpx.objective.set_sense(self.cpx.objective.sense.maximize)

        leaf_names = OrderedDict(
            ((l, j), "l_{}_{}".format(l, j))
            for j in range(ncons)
            for l in range(nleaves)
        )
        mu_names = OrderedDict(
            ((l, i), "u_{}_{}".format(l, i))
            for i in range(nvars)
            for l in range(nleaves)
        )
        nu_names = OrderedDict(
            ((l, i), "v_{}_{}".format(l, i))
            for i in range(nvars)
            for l in range(nleaves)
        )

        pi_names = ["g_{}".format(i) for i in range(nvars + 1)]

        pids = self.cpx.variables.add(
            obj=[1] + [-x for x in sol],
            names=pi_names,
            lb=[-cplex.infinity] * (nvars + 1),
        )
        lids = self.cpx.variables.add(
            names=list(leaf_names.values()), lb=[0] * ncons * nleaves
        )
        muids = self.cpx.variables.add(
            names=list(mu_names.values()), lb=[0] * nvars * nleaves
        )
        nuids = self.cpx.variables.add(
            names=list(nu_names.values()), lb=[0] * nvars * nleaves
        )

        for l in range(nleaves):
            leaf = [leaf_names[l, j] for j in range(ncons)]
            for i in range(nvars):
                names = [pi_names[i + 1]] + leaf + [mu_names[l, i], nu_names[l, i]]

                ma = [-x[i] for x in A]
                coefs = [1] + ma + [-1, 1]
                self.cpx.linear_constraints.add([(names, coefs)], "E", [0])

            var_names = (
                [pi_names[0]]
                + leaf
                + [mu_names[l, i] for i in range(nvars)]
                + [nu_names[l, i] for i in range(nvars)]
            )

            L_t = [-bound[0] for bound in leaves[l]]
            U_t = [bound[1] for bound in leaves[l]]
            self.cpx.linear_constraints.add(
                [(var_names, [1] + [-b_m for b_m in b] + L_t + U_t)], "L", [0]
            )

        # Standard normalisation
        all_vars = list(lids) + list(muids) + list(nuids)
        self.cpx.linear_constraints.add([(all_vars, [1] * len(all_vars))], "E", [1])

    def get_cut(self, sol):
        pi = self.cpx.solution.get_values(list(range(self.nvars)))

        return pi[1:], pi[0]
