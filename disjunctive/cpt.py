import math
from copy import deepcopy


class CPT:
    sigma = 0

    def __init__(self, lbs, ubs):
        self.lbs = lbs
        self.ubs = ubs

        self.left = None
        self.right = None

        self.cuts = []

        self._sigma = deepcopy(CPT.sigma)

        self._check()

    def get_node(self):
        """ Get the current node's bounds. """
        return list(zip(self.lbs, self.ubs))

    def to_list(self):
        return [x.get_node() for x in bfs(self)]

    def __repr__(self):
        return str(self.to_list())

    def branch(self, vid, value):
        """
        Branch on this node with new disjunction.

        TODO Propagate down the tree? For now, we need to call branch on the
        correct node.

        >>> q = CPT([0, 0], [3, 3])
        >>> q.branch(0, 1.5)
        >>> q.is_leaf()
        False

        >>> q.left.is_leaf()
        True

        >>> q.left.lbs, q.left.ubs
        ([0, 0], [1, 3])

        >>> q.right.is_leaf()
        True

        >>> q.right.lbs, q.right.ubs
        ([2, 0], [3, 3])
        """
        lbs = deepcopy(self.lbs)
        ubs = deepcopy(self.ubs)

        assert value <= ubs[vid] and value >= lbs[vid], \
            "Value is out of bounds: {} \\notin [{},{}]".format(
                value, lbs[vid], ubs[vid])

        floor = math.floor(value)

        ubs[vid] = floor
        lbs[vid] = floor + 1

        self.left = CPT(self.lbs, ubs)
        self.right = CPT(lbs, self.ubs)

        # self.left.data = deepcopy(self.data)
        # self.right.data = deepcopy(self.data)

        # base = [0] * len(self.lbs)
        # base[vid] = 1
        # self.left.data._sigma = CPT.sigma
        # self.right.data._sigma = CPT.sigma

    def _check(self):
        assert not any(
            l > u for l, u in zip(self.lbs, self.ubs)
        ), "Bound error: {}".format(list(zip(self.lbs, self.ubs)))

    def is_leaf(self):
        return self.left is None and self.right is None

    def get_leaves(self):
        """
        Return the leaves of the tree rooted at @self.

        >>> q = CPT([0, 0], [3, 3])
        >>> q.branch(0, 1.5)
        >>> q.left.branch(0, 0.5)
        >>> q.get_leaves()
        [[(0, 0), (0, 3)], [(1, 1), (0, 3)], [(2, 3), (0, 3)]]

        >>> q = CPT([0, 0, 0], [2, 2, 2])
        >>> q.branch(0, 1.5)
        >>> q.get_leaves()
        [[(0, 1), (0, 2), (0, 2)], [(2, 2), (0, 2), (0, 2)]]
        """
        if self.is_leaf():
            return self.to_list()

        return self.left.get_leaves() + self.right.get_leaves()

    def at_leaf(self, solution):
        """
        Check whether a solution is *in* a leaf node.

        >>> q = CPT([0, 0], [3, 3])
        >>> q.at_leaf([1.5, 0])
        True

        >>> q.branch(0, 1.5)
        >>> q.at_leaf([0.5, 0.5])
        True

        >>> q.left.branch(0, 0.5)
        >>> q.left.at_leaf([1.5, 1.5])
        False
        """
        return self.find_node(solution).is_leaf()

    def find_node(self, solution, cuts=[]):
        """
        Returns which node the solution *belongs* to.

        NOTE: this only returns the correct cuts if called from the root.

        >>> q = CPT([0, 0], [3, 3])
        >>> q.branch(0, 15 / 18)
        >>> q.at_leaf([2, 2 / 3])
        True
        """
        cuts.extend(self.cuts)
        if self.is_leaf():
            return self

        go_left = all([
            l <= x and x <= u
            for l, x, u in zip(self.left.lbs, solution, self.left.ubs)
        ])

        go_right = all([
            l <= x and x <= u
            for l, x, u in zip(self.right.lbs, solution, self.right.ubs)
        ])

        assert not (go_left and go_right), "Multi-value solution?"

        if go_left:
            return self.left.find_node(solution)

        if go_right:
            return self.right.find_node(solution)

        return self

    def update_sigma(self):
        self._sigma = CPT.sigma

        if not self.is_leaf():
            self.left.update_sigma()
            self.right.update_sigma()

def dfs(node):
    if node is None:
        return []

    return [node] + dfs(node.left) + dfs(node.right)


def bfs(node):
    if node is None:
        return []

    tree = []
    current = [node]
    while current:
        lower = []
        for n in current:
            tree.append(n)
            if not n.is_leaf():
                lower.append(n.left)
                lower.append(n.right)

        current = lower

    return tree


if __name__ == "__main__":
    import doctest

    doctest.testmod()
