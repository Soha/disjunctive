#
# Disjunctive programming solver.
#
# References
#
# 1. Chen, B., Kucukyavuz, S., & Sen, S. (2011).
#    Finite Disjunctive Programming Characterizations for General Mixed-Integer Linear Programs.
#    Operations Research, 59(1), 202–210.
#    https://doi.org/10.1287/opre.1100.0882
# 2. Qi, Y., & Sen, S. (2017).
#    The Ancestral Benders’ cutting plane algorithm with multi-term disjunctions for mixed-integer recourse decisions in stochastic programming.
#    Mathematical Programming, 161(1–2), 193–235.
#    https://doi.org/10.1007/s10107-016-1006-6
#
from .cpt import CPT, dfs
from numpy import math


def round_sol(sol, precision=6):
    return [round(x, precision) for x in sol]


def create_tree(model=None, lbs=None, ubs=None):
    """ Create a Cutting-Plate-Tree. """
    if model is None:
        assert lbs is not None and ubs is not None, "Need explicit lb/ub without model."
    else:
        lbs = model.variables.get_lower_bounds()
        ubs = model.variables.get_upper_bounds()

    return CPT(lbs, ubs)


def find_fractional(solution, precision=1e-6):
    """
    Find the first (lexicographic order) variable with fractional value in a
    solution.

    >>> find_fractional([1.875, 1.0])
    0

    >>> find_fractional([2, 2 / 3])
    1

    >>> find_fractional([1, 1])

    >>> find_fractional([0.9999999999999998, 1.5833333333333335])
    1
    """
    for i, v in enumerate(solution):
        if not math.isclose(v, round(v), abs_tol=precision):
            return i

    # TODO Exception?
    return None

