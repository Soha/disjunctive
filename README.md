# Disjunctive Programming using a Cutting Plane Tree

Implementation of disjunctive programming, a convexification scheme.
We use a Cutting-Plane Tree to ensure convergence.

## References

1. Chen, B., Küçükyavuz, S., & Sen, S. (2012). A computational study of the cutting plane tree algorithm for general mixed-integer linear programs. Operations Research Letters, 40(1), 15–19. https://doi.org/10.1016/j.orl.2011.10.009
2. Fischetti, M., Lodi, A., & Tramontani, A. (2011). On the separation of disjunctive cuts. Mathematical Programming, 128(1–2), 205–230. https://doi.org/10.1007/s10107-009-0300-y
3. Balas, E. (1979). Disjunctive Programming. In Discrete Applied Mathematics (Vol. 5, pp. 3–51). https://doi.org/10.1016/S0167-5060(08)70342-X
4. Chen, B., Küçükyavuz, S., & Sen, S. (2011). Finite Disjunctive Programming Characterizations for General Mixed-Integer Linear Programs. Operations Research, 59(1), 202–210. https://doi.org/10.1287/opre.1100.0882
