# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='disjunctive',
    version='0.1',
    description='Disjunctive programming using a cutting-plane tree',
    long_description=readme,
    author='Arthur Mahéo',
    author_email='arthur.maheo@monash.edu.au',
    url='https://gitlab.com/Soha/disjunctive.git',
    license=license,
    python_requires='==3.5.*',    # Because of CPLEX
    packages=find_packages(exclude=('tests', 'docs')),
    install_requires=['cplex'],
    extras_require={
        'dev': ['yapf', 'pytest'],
        'test': ['pytest']})
