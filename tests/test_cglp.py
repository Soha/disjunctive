import cplex
import math

from disjunctive.utils import find_fractional, create_tree, round_sol


def iteration(nvars, sol, A, b, pi, pi_0):
    ncons = len(b) + 1

    assert len(pi) == nvars - 1, pi
    assert len(A) == len(b)

    cglp = cplex.Cplex()
    gids = cglp.variables.add(
        obj=[-1] + sol,
        names=["g_{}".format(i) for i in range(nvars)],
        lb=[-cplex.infinity] * nvars,
    )
    uids = cglp.variables.add(
        names=["u_{}".format(i) for i in range(ncons)], lb=[0] * ncons
    )
    vids = cglp.variables.add(
        names=["v_{}".format(i) for i in range(ncons)], lb=[0] * ncons
    )

    for i in range(nvars - 1):
        ma = [-x[i] for x in A]
        # \pi is an elementary disjunction: 1 branching variable and 0 otw.
        #
        # \gamma + u constraint
        cglp.linear_constraints.add(
            [([gids[i + 1]] + list(uids), [1] + [pi[i]] + ma)], "E", [0]
        )
        # \gamma + v constraint, change sense of \pi
        cglp.linear_constraints.add(
            [([gids[i + 1]] + list(vids), [1] + [-pi[i]] + ma)], "E", [0]
        )

    mb = [-x for x in b]
    # \gamma_0 + u constraint
    cglp.linear_constraints.add([([gids[0]] + list(uids), [1] + [pi_0] + mb)], "E", [0])
    # \gamma_0 + v constraint
    cglp.linear_constraints.add(
        [([gids[0]] + list(vids), [1] + [-(pi_0 + 1)] + mb)], "E", [0]
    )
    # Cut normalisation
    #
    # Standard Normalisation Condition (SNC)
    cglp.linear_constraints.add(
        [(list(uids) + list(vids), [1] * (len(uids) * 2))], "E", [1]
    )

    # Trivial
    # cglp.linear_constraints.add([([uids[0], vids[0]], [1, 1])], "E", [1])

    cglp.solve()

    # assert cglp.solution.get_status() == cglp.solution.status.optimal

    return cglp


def test_example_1():
    """ Example 1 from [1], we ignore the tree correctness. """
    g = cplex.Cplex()
    # Needs to be in normal form
    A = [[-8, -12], [-8, -3]]
    b = [-27, -18]

    xids = g.variables.add(obj=[-1, -1], lb=[0, 0], ub=[3, 3], types="CC")
    lhs = [(xids, A[0]), (xids, A[1])]
    cids = g.linear_constraints.add(lin_expr=lhs, rhs=b, senses="GG")
    nvars = len(xids) + 1

    # We want the LP relaxation of the problem. Has to be done *after* adding
    # the variables...
    g.set_problem_type(g.problem_type.LP)
    g.objective.set_sense(g.objective.sense.minimize)
    # g.presolve.presolve(0)
    g.parameters.preprocessing.presolve.set(0)

    # cglp = CGLP(xids)
    # gids, uids, vids = cglp.get_ids()

    # Tree root
    n1 = create_tree(g)

    # k=1
    n2 = [(0, 1), (0, 3)]
    n3 = [(2, 3), (0, 3)]

    # Parameterise CGLP
    k_1 = [15 / 8, 1]
    cglp = iteration(nvars, k_1, A, b, [1, 0], math.floor(k_1[0]))
    coefs = cglp.solution.get_values([0, 1, 2])

    cglp.write("cglp-1.lp")

    g.linear_constraints.add([(xids, coefs[1:])], "G", [coefs[0]])
    g.solve()

    # Update the constraint matrix
    A.append(coefs[1:])
    b.append(coefs[0])

    assert g.solution.get_status() == g.solution.status.optimal
    assert round_sol(g.solution.get_values()) != round_sol(k_1)

    k_2 = [2, 2 / 3]
    lhs = sum([x * g for x, g in zip(k_2, coefs[1:])])
    assert (
        math.isclose(lhs, coefs[0], abs_tol=1e-06) or lhs <= coefs[0]
    ), "{} <= {}".format(list(zip(k_2, coefs[1:])), coefs[0])

    cglp = iteration(nvars, k_2, A, b, [0, 1], math.floor(k_2[1]))
    coefs = cglp.solution.get_values([0, 1, 2])
    cglp.write("cglp-2.lp")

    g.linear_constraints.add([(xids, coefs[1:])], "G", [coefs[0]])
    g.solve()

    k_3 = [1, 19 / 12]

    assert sum([x * g for x, g in zip(k_3, coefs[1:])]) > coefs[0], "{} <= {}".format(
        list(zip(k_3, coefs[1:])), coefs[0]
    )

    # At this point, the solutions diverge

    return g
