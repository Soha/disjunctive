import cplex
from disjunctive.utils import find_fractional, create_tree, round_sol


def make_model():
    g = cplex.Cplex()
    g.objective.set_sense(g.objective.sense.minimize)
    g.presolve.presolve(g.presolve.method.none)

    return g


def iteration(g, tree, val, which, leaves=[], at_leaf=True):
    # Solve the current LP relaxation
    g.solve()
    assert g.solution.get_status() == g.solution.status.optimal

    # Check solution
    sol = g.solution.get_values()
    assert round_sol(sol) == round_sol(val)

    # Find branching variable
    vid = find_fractional(sol)
    assert vid == which

    # Find node where the solution is at
    node = tree.find_node(sol)
    assert node.is_leaf() == at_leaf

    # Branch
    if node.is_leaf():
        node.branch(vid, sol[vid])
        assert node.get_leaves() == leaves


def test_example_1():
    """ Example 1 from [1], we ignore the cut generation procedure. """

    g = make_model()
    # We want the LP relaxation of the problem
    vids = g.variables.add(obj=[-1, -1], lb=[0, 0], ub=[3, 3], types="CC")
    lhs = [(vids, [8, 12]), (vids, [8, 3])]
    cids = g.linear_constraints.add(lin_expr=lhs, rhs=[27, 18], senses="LL")
    # Has to be done *after* adding the variables...
    g.set_problem_type(g.problem_type.LP)
    n1 = create_tree(g)

    # k=1
    n2 = [(0, 1), (0, 3)]
    n3 = [(2, 3), (0, 3)]
    iteration(g, n1, [15 / 8, 1], 0, [n2, n3])

    # k=2
    g.linear_constraints.add([(vids, [11 / 12, 1])], "L", [5 / 2])
    n4 = [(2, 3), (0, 0)]
    iteration(g, n1, [2, 2 / 3], 1, [n4, [(2, 3), (1, 3)]])

    # k=3
    g.linear_constraints.add([(vids, [1, 15 / 19])], "L", [9 / 4])
    n5 = [(0, 1), (0, 1)]
    n6 = [(0, 1), (2, 3)]
    iteration(g, n1, [1, 19 / 12], 1, [n5, n6])

    # k=4
    g.linear_constraints.add([(vids, [1, 15 / 16])], "L", [9 / 4])
    test_sol = [3 / 8, 2]
    n7 = [(0, 0), (2, 3)]
    iteration(g, n1, test_sol, 0, [n7, [(1, 1), (2, 3)]])

    # Test duplicate solution
    g.solve()
    assert round_sol(g.solution.get_values()) == round_sol(test_sol)
    assert not n1.at_leaf(test_sol)

    # k=5 may have an error, so doing k=6 first
    g.linear_constraints.add([(vids, [1, 1])], "L", [9 / 4])
    n9 = [(0, 0), (2, 2)]
    iteration(g, n1, [0, 9 / 4], 1, [n9, [(0, 0), (3, 3)]])

    # k=6 actual k=5, have to swap the coefs in the cut
    # g.linear_constraints.add([(vids, [8, 9])], "L", [9 / 4])
    # n8 = [(2, 2), (0, 0)]
    # iteration(g, n1, [9 / 32, 0], 0, [n8, [(3, 3), (0, 0)]])

    # k=7
    g.linear_constraints.add([(vids, [1, 1])], "L", [2])
    g.solve()
    assert round_sol(g.solution.get_values()) == round_sol([0, 2])


def test_example_2():
    g = make_model()
    vids = g.variables.add([0, 0, -1], [0, 0, 0], [2, 2, 2], "CCC")
    lhs = [([0, 2], [1, -1]), ([1, 2], [1, -1]), ([vids, [1, 1, 2]])]
    rhs = [0, 0, 2]
    cids = g.linear_constraints.add(lhs, "GGL", rhs)
    g.set_problem_type(g.problem_type.LP)

    # x_3 is a real variable
    n1 = create_tree(lbs=[0, 0], ubs=[2, 2])

    # k=1
    n2 = [(0, 0), (0, 2)]
    n3 = [(1, 2), (0, 2)]
    iteration(g, n1, [1 / 2, 1 / 2, 1 / 2], 0, [n2, n3])

    # k=2
    g.linear_constraints.add([([0, 2], [1, -3])], "G", [0])
    n4 = [(1, 2), (0, 0)]
    n5 = [(1, 2), (1, 2)]
    iteration(g, n1, [1, 1 / 3, 1 / 3], 1, [n4, n5])

    # k=3
    g.linear_constraints.add([([2], [1])], "L", [0])
    g.solve()
    # Symmetric solutions exist
    assert g.solution.get_objective_value() == 0
    assert find_fractional(g.solution.get_values()) is None


def test_example_3():
    g = make_model()
    vids = g.variables.add([0, 0, -1], [0, 0, 0], [1, 1, 1], "CCC")
    lhs = [(vids, [1, 2, -2]), (vids, [2, 2, -3]), (vids, [2, 1, -2]), ([0, 1], [2, 2])]
    cids = g.linear_constraints.add(lin_expr=lhs, rhs=[0, 0, 0, 3], senses="GGGL")
    g.set_problem_type(g.problem_type.LP)

    n1 = create_tree(g)

    # k=1
    n2 = [(0, 1), (0, 0), (0, 1)]
    n3 = [(0, 1), (1, 1), (0, 1)]
    iteration(g, n1, [1, 1 / 2, 1], 1, [n2, n3])

    # k=2
    g.linear_constraints.add([([0, 1], [1, 1 / 2])], "L", [1])
    n4 = [(0, 0), (1, 1), (0, 1)]
    iteration(g, n1, [1 / 2, 1, 1], 0, [n4, [(1, 1), (1, 1), (0, 1)]])

    # k=3
    g.linear_constraints.add([(vids, [1, 1, -2])], "G", [0])
    iteration(g, n1, [1 / 2, 1, 3 / 4], 0, at_leaf=False)

    # k=5
    n6 = [(0, 0), (1, 1), (0, 0)]
    g.linear_constraints.add([([0, 1], [1, 1])], "L", [1])
    iteration(g, n1, [0, 1, 1 / 2], 2, [n6, [(0, 0), (1, 1), (1, 1)]])

    # k=4
    # g.linear_constraints.add([([1, 2], [1, -2])], "G", [0])
    g.linear_constraints.add([([2], [1])], "L", [0])
    # n5 = [(0, 1), (0, 0), (0, 0)]
    # iteration(g, n1, [1, 0, 1 / 2], 2, [n5, [(0, 1), (0, 0), (1, 1)]])
    # Breaks, so we find another integer solution

    # k=6
    g.solve()
    assert round_sol(g.solution.get_values()) == round_sol([0, 1, 0])
