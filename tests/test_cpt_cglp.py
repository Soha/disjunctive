import cplex
import math
import numpy as np

from disjunctive.utils import find_fractional, create_tree, round_sol
from disjunctive.cpt import CPT
from disjunctive.cglp import OneStep, M1NC, Leaf


def test_example_1():
    """ Solve example 1 from [1] using CPT + CGLP. """
    run_example_1(Leaf)
    run_example_1(M1NC)


def test_example_2():
    """ Solve example 2 from [1] using CPT + CGLP. """
    run_example_2(Leaf)
    run_example_2(M1NC)


def test_example_3():
    """ Solve example 3 from [1] using CPT + CGLP. """
    run_example_3(Leaf)
    run_example_3(M1NC)
    # run_example_1(OneStep)


def run_example_1(cglp):
    g = cplex.Cplex()
    # Needs to be in normal form
    A = [[-8, -12], [-8, -3]]
    b = [-27, -18]

    xids = g.variables.add(obj=[-1, -1], lb=[0, 0], ub=[3, 3], types="CC")
    lhs = [(xids, A[0]), (xids, A[1])]
    cids = g.linear_constraints.add(lin_expr=lhs, rhs=b, senses="GG")
    nvars = len(xids)

    # We want the LP relaxation of the problem. Has to be done *after* adding
    # the variables...
    g.set_problem_type(g.problem_type.LP)
    g.objective.set_sense(g.objective.sense.minimize)
    g.parameters.preprocessing.presolve.set(0)

    # Tree root
    root = create_tree(g)
    A_k = []
    b_k = []

    g.solve()  # Initial solution
    while True:
        sol = round_sol(g.solution.get_values())[:nvars]
        index = find_fractional(sol)
        if index is None:  # Integer solution
            break

        node = root.find_node(sol)
        CPT.sigma += 1

        if node.is_leaf():
            node.branch(index, sol[index])
            A_m = A + A_k
            b_m = b + b_k
        else:
            node.left.update_sigma()
            node.right.update_sigma()
            A_m = A + A_k[: node._sigma]
            b_m = b + b_k[: node._sigma]

        L_k = root.get_leaves()
        cut = cglp(sol, A_m, b_m, L_k)
        cut.solve()
        coefs, rhs = cut.get_cut(sol)

        # Update the constraint matrix
        A_k.append(coefs)
        # Update the RHS
        b_k.append(rhs)

        g.linear_constraints.add([(xids, A_k[-1])], "G", [b_k[-1]])
        g.solve()

        assert g.solution.get_status() == g.solution.status.optimal, (
            "LP is " + g.solution.get_status_string()
        )
        assert round_sol(g.solution.get_values()) != round_sol(sol)

    assert g.solution.get_objective_value() == -2


def run_example_2(cglp):
    """ Solve example 2 from [1] using CPT + CGLP. """
    g = cplex.Cplex()
    # Needs to be in normal form
    A = [[1, 0, -1], [0, 1, -1], [-1, -1, -2]]
    b = [0, 0, -2]

    # Need to have a concrete UB instead of +inf
    xids = g.variables.add(obj=[0, 0, -1], lb=[0, 0, 0], ub=[2, 2, 2], types="CCC")
    lhs = [(xids, A[0]), (xids, A[1]), (xids, A[2])]
    cids = g.linear_constraints.add(lin_expr=lhs, rhs=b, senses="GGG")

    # We want the LP relaxation of the problem. Has to be done *after* adding
    # the variables...
    g.set_problem_type(g.problem_type.LP)
    g.objective.set_sense(g.objective.sense.minimize)
    g.parameters.preprocessing.presolve.set(0)

    # Tree root
    root = create_tree(g)
    A_k = []
    b_k = []

    g.solve()  # Initial solution
    while True:
        sol = round_sol(g.solution.get_values())
        index = find_fractional(sol)
        if index is None or index > 1:  # Integer solution
            break

        node = root.find_node(sol)
        CPT.sigma += 1

        if node.is_leaf():
            node.branch(index, sol[index])
            A_m = A + A_k
            b_m = b + b_k
        else:
            node.left.update_sigma()
            node.right.update_sigma()
            A_m = A + A_k[: node._sigma]
            b_m = b + b_k[: node._sigma]

        L_k = root.get_leaves()
        cut = cglp(sol, A_m, b_m, L_k)
        cut.solve()
        # Return x_3
        coefs, rhs = cut.get_cut(sol)

        # Update the constraint matrix
        A_k.append(coefs)
        # Update the RHS
        b_k.append(rhs)

        g.linear_constraints.add([(xids, coefs)], "G", [rhs])
        g.solve()

        assert g.solution.get_status() == g.solution.status.optimal
        assert round_sol(g.solution.get_values()) != round_sol(sol)

    assert math.isclose(g.solution.get_objective_value(), 0, abs_tol=1e-6)


def run_example_3(cglp):
    """ Solve example 3 from [1] using CPT + CGLP. """
    g = cplex.Cplex()
    # Needs to be in normal form
    A = [[1, 2, -2], [2, 2, -3], [2, 1, -2], [-2, -2, 0]]
    b = [0, 0, 0, -3]

    xids = g.variables.add(obj=[0, 0, -1], lb=[0, 0, 0], ub=[1, 1, 1], types="CCC")
    lhs = [(xids, a) for a in A]
    cids = g.linear_constraints.add(lin_expr=lhs, rhs=b, senses="GGGG")
    nvars = len(xids)

    # We want the LP relaxation of the problem. Has to be done *after* adding
    # the variables...
    g.set_problem_type(g.problem_type.LP)
    g.objective.set_sense(g.objective.sense.minimize)
    g.parameters.preprocessing.presolve.set(0)

    # Tree root
    root = create_tree(g)
    A_k = []
    b_k = []

    g.solve()  # Initial solution
    while True:
        sol = round_sol(g.solution.get_values())[:nvars]
        index = find_fractional(sol)
        if index is None:  # Integer solution
            break

        node = root.find_node(sol)
        CPT.sigma += 1

        if node.is_leaf():
            node.branch(index, sol[index])
            A_m = A + A_k
            b_m = b + b_k
        else:
            node.left.update_sigma()
            node.right.update_sigma()
            A_m = A + A_k[: node._sigma]
            b_m = b + b_k[: node._sigma]

        L_k = root.get_leaves()
        cut = cglp(sol, A_m, b_m, L_k)
        cut.solve()
        coefs, rhs = cut.get_cut(sol)

        # Update the constraint matrix
        A_k.append(coefs)
        # RHS = 1 + x^k \cdot \pi^T
        b_k.append(rhs)

        g.linear_constraints.add([(xids, A_k[-1])], "G", [b_k[-1]])
        g.solve()

        assert g.solution.get_status() == g.solution.status.optimal
        assert round_sol(g.solution.get_values()) != round_sol(sol)

    assert g.solution.get_objective_value() == 0
